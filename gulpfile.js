const gulp = require('gulp'); //npm install --save-dev gulp
const sass = require('gulp-sass'); // npm install --save-dev gulp-sass
const autoprefixer = require('gulp-autoprefixer'); // npm install --save-dev gulp-autoprefixer

function css() {
    return gulp
        .src(['sass/style.scss', 'sass/config/*.scss'])
        .pipe(autoprefixer({
            overrideBrowserslist : ['last 2 versions'],
            cascade: false
        }))
        .pipe(sass({
            outputStyle: 'expanded', // nested, compact, compressed
        }))
        .pipe( gulp.dest('css') );
}

function watchFiles() {
    gulp.watch(['sass/style.scss', 'sass/config/*.scss'], css); 
}


// Registrar funciones como tareas
gulp.task( 'css', css );
gulp.task('watch', gulp.parallel( watchFiles) );